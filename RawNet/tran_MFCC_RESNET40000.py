import ASVBaselineTool.trans_tool as tran
import Config.attackconfig as Config
import torch
from MFCC_RESNET40000.model import Model_zoo

model_path = Config.MFCC_RESNET40000_model
advsavepath =Config.RawNet_PGDAdvsetPath
cleandatapath = Config.TPATH
trainlabelpath = Config.TLABEL

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))


A = tran.CalClip40000TransAtKSuccess(model,64600,40000,advsavepath,cleandatapath,trainlabelpath,feature= "mfcc_40",desc="MFCC_RESNET40000")
print(A)