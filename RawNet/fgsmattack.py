import numpy as np
import ASVBaselineTool.FGSMATTACK as attack
import Config.attackconfig as Config
import Config.globalconfig as Gconfig
import torch
from RawNet.model import RawNet
import yaml
model_path = Config.RawNet_model
labelPath = Config.TLABEL
datasetPath = Config.TPATH
AdvsetPath = Config.RawNet_FGSMAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
yaml_path = Gconfig.RAW_YAML_CONFIG_PATH
with open(yaml_path, 'r') as f_yaml:
    parser1 = yaml.load(f_yaml, Loader=yaml.FullLoader)
model = RawNet(parser1['model'], device)
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.FRAMEFGSM(model,labelPath,datasetPath,AdvsetPath)
feature = "raw"
for eps in np.linspace(0.02,0.03,10):
    a.Attack(feature,eps=eps)
