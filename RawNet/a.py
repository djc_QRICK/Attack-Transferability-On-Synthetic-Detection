import ASVBaselineTool.mi_fgsm as attack
import Config.attackconfig as Config
import Config.globalconfig as Gconfig
import torch
from RawNet.model import RawNet
import yaml

model_path = Config.RawNet_model
labelPath = Config.TLABEL
datasetPath = Config.TPATH
AdvsetPath = Config.RawNet_MIFGSMAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
yaml_path = Gconfig.RAW_YAML_CONFIG_PATH
with open(yaml_path, 'r') as f_yaml:
    parser1 = yaml.load(f_yaml, Loader=yaml.FullLoader)
model = RawNet(parser1['model'], device)
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.MIFGSM(model,labelPath,datasetPath,AdvsetPath,steps=40,eps=0.08)
feature = "raw"
a.Attack(feature,alpha=0.007,stop=0.05)
