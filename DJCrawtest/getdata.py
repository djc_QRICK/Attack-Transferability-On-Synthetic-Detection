import numpy as np
def get646_4_646_and_646_48_646(x):
    x1=x[:40000]
    x1=np.concatenate((x1,x1[:24600]),dim=0)
    x2=x[:48000]
    x2 = np.concatenate((x2, x2[:16600]), dim=0)
    return x1,x2

def get48_646_and_4_646(x48,x4):
    x48_646=np.concatenate((x48,x48[:16600]),dim=0)
    x4_646 = np.concatenate((x4, x4[:24600]), dim=0)
    return x48_646,x4_646
