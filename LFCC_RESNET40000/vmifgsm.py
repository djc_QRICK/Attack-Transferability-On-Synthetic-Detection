import ASVBaselineTool.VMIFGSM as attack
import Config.attackconfig as Config
import torch
from LFCC_RESNET40000.model import Model_zoo

model_path = Config.LFCC_RESNET40000_model
labelPath = Config.VMILABEL
datasetPath = Config.VMIPATH
AdvsetPath = Config.LFCC_RESNET40000_VMIFGSMAdvsetPath1

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.VMIFGSM(model,labelPath,datasetPath,AdvsetPath,steps=100,eps=0.08)
feature = "lfcc_70"
a.Attack(feature,alpha=0.0001,stop=0.05)

# 测试alpha=0.001  攻击spec2048-40000 raw40000 lfcc64600 的迁移性
# 测试alpha=0.005  攻击spec2048-40000 raw40000 lfcc64600 的迁移性
# a=0.002