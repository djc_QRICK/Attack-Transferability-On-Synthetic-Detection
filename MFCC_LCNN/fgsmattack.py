import numpy as np
import ASVBaselineTool.FGSMATTACK as attack
import Config.attackconfig as Config
import torch
from MFCC_LCNN.model import Model_zoo

model_path = Config.MFCC_LCNN_model
labelPath = Config.TLABEL
datasetPath = Config.TPATH
AdvsetPath = Config.MFCC_LCNN_FGSMAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("lcnn_mfcc")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.FRAMEFGSM(model,labelPath,datasetPath,AdvsetPath)
feature = "mfcc_40"
for eps in np.linspace(0.01,0.05,33):
    a.Attack(feature,eps=eps)
