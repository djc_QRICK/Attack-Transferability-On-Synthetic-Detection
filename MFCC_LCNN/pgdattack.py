import ASVBaselineTool.PGDATTACK as attack
import Config.attackconfig as Config
import torch
from  MFCC_LCNN.model import Model_zoo

model_path = Config.MFCC_LCNN_model
labelPath = Config.TLABEL
datasetPath = Config.TPATH
AdvsetPath = Config.MFCC_LCNN_PGDAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("lcnn_mfcc")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.PGD(model,labelPath,datasetPath,AdvsetPath,steps=40,eps=0.08)
feature = "mfcc_40"
a.Attack(feature,alpha=0.001,stop=0.05)
