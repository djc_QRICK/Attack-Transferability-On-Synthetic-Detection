import torch
import soundfile as sf
import numpy as np
import os
import librosa
from CQCC import cqcc as cqcc_func
import pickle


def cqcc_feature(data_path,label_path, is_eval=False):
    if (is_eval==False):
        with open(label_path, "r") as f:
            all_label = f.readlines()
        label_fnames = [item.split(" ")[0] for item in all_label]
        a = 0
        for i in label_fnames:
            X, fs = sf.read(os.path.join(data_path, i))
            B = 12
            fmax = fs / 2
            fmin = fmax // 2 ** 9
            d = 16
            cf = 19
            ZsdD = 'ZsdD'
            cqcc1 = cqcc_func.cqcc(X, fs, B, fmax, fmin, d, cf, ZsdD)
            name = i.split(".")[0]
            a = a+1
            print(a)
            with open(r'F:\csy\ASVNormClip64600Dataset\train_kpl_12\{}.pkl'.format(name), 'wb') as f:
                pickle.dump(cqcc1, f)


data_path = r'F:\csy\ASVNormClip64600Dataset\train'
labe_path = r'F:\csy\ASVNormClip64600Dataset\train_label.txt'
# kpl_path = r'F:\csy\ASVNormClip64600Dataset\dev_kpl'
cqcc_feature(data_path,labe_path)






# x=torch.randn(1,64000)
#
# def DCT(x):
#     N = len(x)
#     X = np.zeros(N)
#     ts = np.array([i for i in range(N)])
#     C = np.ones(N)
#     C[0] = np.sqrt(2) / 2
#     for k in range(N):
#         X[k] = np.sqrt(2 / N) * np.sum(C[k] * np.multiply(x, np.cos((2 * ts + 1) * k * np.pi / 2 / N)))
#     return X
#
#
# data ,fs = sf.read(path,16000)
# b = dct(data,norm ='ortho')
# A= DCT(data)
# print(A)
# print(b)

