import Config.globalconfig as Config
import torch
import yaml
from RawNet.model import RawNet
from torch.utils.data import DataLoader
from LFCC_RESNET.model import Model_zoo
from RawNet.data_utils import genSpoof_list, Dataset_ASVspoof2019_train
from RawNet.train_model import evaluate_accuracy,train_epoch
import os
import soundfile as sf
import ASVBaselineTool.feature_tools as ft

model_path = Config.RESNETLCNN40000_MS_CHOICE_PATH
labelPath = Config.TLABEL
datasetPath = Config.TPATH
batch_size= 1

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
yaml_path = Config.RAW_YAML_CONFIG_PATH
with open(yaml_path, 'r') as f_yaml:
    parser1 = yaml.load(f_yaml, Loader=yaml.FullLoader)
    #
model = RawNet(parser1['model'], device)
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

data_train_label, data_train_file = genSpoof_list(labelPath, is_eval=False)
print('no. of training trials', len(data_train_file))
train_set = Dataset_ASVspoof2019_train(list_IDs=data_train_file,
                                        labels=data_train_label,
                                        base_dir=datasetPath)
train_loader = DataLoader(train_set, batch_size=batch_size,
                        shuffle=False, drop_last=True)


a = evaluate_accuracy(train_loader,model,device)
print(a)

