import ASVBaselineTool.PGDATTACK as attack
import Config.attackconfig as Config
import torch
from  SPEC_LCNN.model import Model_zoo

model_path = Config.SPEC_LCNN_model
labelPath = Config.TLABEL
datasetPath = Config.TPATH
AdvsetPath = Config.SPEC_LCNN_PGDAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("lcnn_spec")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.PGD(model,labelPath,datasetPath,AdvsetPath,steps=40,eps=0.08)
feature = "spec_2048"
a.Attack(feature,alpha=0.001,stop=0.05)
