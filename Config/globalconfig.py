#训练集路径
TPATH64600=r"F:\csy\ASVNormClip64600Dataset\train"
TLABEL64600=r"F:\csy\ASVNormClip64600Dataset\train_label.txt"
TPATH48000=r"F:\csy\ASVNormClip48000Dataset\train"
TLABEL48000=r"F:\csy\ASVNormClip48000Dataset\train_label.txt"
TPATH40000=r"F:\csy\ASVNormClip40000Dataset\train"
TLABEL40000=r"F:\csy\ASVNormClip40000Dataset\train_label.txt"

TPATHKPL64600=r"F:\csy\ASVNormClip64600Dataset\train_kpl_12"
#验证集路径
EPATH64600=r"F:\csy\ASVNormClip64600Dataset\dev"
ELABEL64600=r"F:\csy\ASVNormClip64600Dataset\dev_label.txt"
EPATH48000=r"F:\csy\ASVNormClip48000Dataset\dev"
ELABEL48000=r"F:\csy\ASVNormClip48000Dataset\dev_label.txt"
EPATH40000=r"F:\csy\ASVNormClip40000Dataset\dev"
ELABEL40000=r"F:\csy\ASVNormClip40000Dataset\dev_label.txt"

EPATHKPL64600=r"F:\csy\ASVNormClip64600Dataset\dev_kpl_12"
"""
======================================================LMCC==============================================
"""


#LMCC_LCNN  参数配置
LMLCNN_MS_BATCH_SIZE=16
LMLCNN_MS_NUM_EPOCHS=20
LMLCNN_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_LFCC"
LMLCNN_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_LFCC\WCE_30_16_0.0001\epoch_29_99.1_99.1.pth"


#LM60CC_LCNN  参数配置
LM60LCNN_MS_BATCH_SIZE=16
LM60LCNN_MS_NUM_EPOCHS=20
LM60LCNN_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_LFCC60"
LM60LCNN_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_LFCC60\WCE_20_16_0.0001\epoch_29_99.1_99.1.pth"


#LMCC_RESNET  参数配置
RESNETLCNN_MS_BATCH_SIZE=16
RESNETLCNN_MS_NUM_EPOCHS=20
RESNETLCNN_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_LFCC"
RESNETLCNN_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_LFCC\WCE_20_16_0.0001\epoch_19_99.9_99.8.pth"

#CQCC_RESNET  参数配置
RESNETCQCC_MS_BATCH_SIZE=16
RESNETCQCC_MS_NUM_EPOCHS=20
RESNETCQCC_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_CQCC"
RESNETCQCC_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_LFCC\WCE_20_16_0.0001\epoch_19_99.9_99.8.pth"


#LMCC_RESNET48000  参数配置
RESNETLCNN48000_MS_BATCH_SIZE=16
RESNETLCNN48000_MS_NUM_EPOCHS=20
RESNETLCNN48000_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET48000_LFCC"
RESNETLCNN48000_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_LFCC\WCE_20_16_0.0001\epoch_19_99.9_99.8.pth"

#LMCC_RESNET40000  参数配置
RESNETLCNN40000_MS_BATCH_SIZE=16
RESNETLCNN40000_MS_NUM_EPOCHS=20
RESNETLCNN40000_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_LFCC"
RESNETLCNN40000_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_LFCC\WCE_20_16_0.0001\epoch_10_99.7_98.7.pth"

#LMCC60_RESNET  参数配置
RESNETLCNN60_MS_BATCH_SIZE=16
RESNETLCNN60_MS_NUM_EPOCHS=20
RESNETLCNN60_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_LFCC60"
RESNETLCNN60_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_LFCC60\WCE_20_16_0.0001\epoch_19_99.9_99.6.pth"
"""
======================================================SPEC============================================
"""

#SPEC_LCNN
SPECLCNN_MS_BATCH_SIZE=12
SPECLCNN_MS_NUM_EPOCHS=20
SPECLCNN_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_SPEC"
SPECLCNN_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_SPEC\WCE_30_12_0.0001\epoch_10_99.0_99.6.pth"


#SPEC_RESNET
SPECRESNET_MS_BATCH_SIZE=12
SPECRESNET_MS_NUM_EPOCHS=20
SPECRESNET_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_SPEC"
SPECRESNET_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_SPEC\WCE_20_12_0.0001\epoch_19_99.9_99.9.pth"

#SPEC_RESNET_NEW
SPECRESNET_NEW_MS_BATCH_SIZE=12
SPECRESNET_NEW_MS_NUM_EPOCHS=20
SPECRESNET_NEW_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_SPEC_NEW"
SPECRESNET_NEW_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_SPEC_NEW\WCE_20_12_0.0001\epoch_19_99.9_99.9.pth"

#SPEC_RESNET48000
SPECRESNET48000_MS_BATCH_SIZE=12
SPECRESNET48000_MS_NUM_EPOCHS=20
SPECRESNET48000_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET48000_SPEC"
SPECRESNET48000_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET48000_SPEC\WCE_20_12_0.0001\epoch_10_99.9_99.9.pth"

#SPEC_RESNET48000
SPECRESNET40000_MS_BATCH_SIZE=12
SPECRESNET40000_MS_NUM_EPOCHS=20
SPECRESNET40000_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_SPEC"
SPECRESNET40000_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_SPEC\WCE_20_12_0.0001\epoch_2_99.5_99.7.pth"


#SPEC1024_LCNN
SPEC1024LCNN_MS_BATCH_SIZE=12
SPEC1024LCNN_MS_NUM_EPOCHS=20
SPEC1024LCNN_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_SPEC1024"
SPEC1024LCNN_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_SPEC1024\WCE_20_12_0.0001\epoch_19_86.7_89.7.pth"

#SPEC1024_RESNET
SPEC1024RESNET_MS_BATCH_SIZE=12
SPEC1024RESNET_MS_NUM_EPOCHS=20
SPEC1024RESNET_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_SPEC1024"
SPEC1024RESNET_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_SPEC1024\WCE_20_12_0.0001\epoch_19_99.9_99.9.pth"

"""
======================================================MFCC============================================
"""

#MFCC_LCNN
MFCCLCNN_MS_BATCH_SIZE=16
MFCCLCNN_MS_NUM_EPOCHS=20
MFCCLCNN_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_MFCC"
MFCCLCNN_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_MFCC\WCE_20_16_0.0001\epoch_19_98.6_98.8.pth"


#MFCC80_LCNN
MFCC80LCNN_MS_BATCH_SIZE=16
MFCC80LCNN_MS_NUM_EPOCHS=20
MFCC80LCNN_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_MFCC80"
MFCC80LCNN_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_MFCC80\WCE_20_16_0.0001\epoch_19_98.6_98.8.pth"


#MFCC_RESNET
MFCCRESNET_MS_BATCH_SIZE=16
MFCCRESNET_MS_NUM_EPOCHS=20
MFCCRESNET_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_MFCC"
MFCCRESNET_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_MFCC\WCE_20_16_0.0001\epoch_19_99.8_99.7.pth"

#MFCC_RESNET48000
MFCCRESNET48000_MS_BATCH_SIZE=16
MFCCRESNET48000_MS_NUM_EPOCHS=20
MFCCRESNET48000_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET48000_MFCC"
MFCCRESNET48000_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET48000_MFCC\WCE_20_16_0.0001\epoch_5_99.6_99.7.pth"

#MFCC_RESNET40000
MFCCRESNET40000_MS_BATCH_SIZE=16
MFCCRESNET40000_MS_NUM_EPOCHS=20
MFCCRESNET40000_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_MFCC"
MFCCRESNET40000_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_MFCC\WCE_20_16_0.0001\epoch_10_99.6_99.7.pth"

#MFCC80_RESNET
MFCC80RESNET_MS_BATCH_SIZE=16
MFCC80RESNET_MS_NUM_EPOCHS=20
MFCC80RESNET_MS_SAVE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_MFCC80"
MFCC80RESNET_MS_CHOICE_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_MFCC80\WCE_20_16_0.0001\epoch_19_99.8_99.7.pth"
"""
======================================================RAWNET============================================
"""

#RawNet 模型保存 文件夹
RAW_MS_BATCH_SIZE=32
RAW_MS_NUM_EPOCHS=30
RAW_YAML_CONFIG_PATH="F:\Attack-Transferability-On-Synthetic-Detection\RawNet\model_config_RawNet.yaml"
RAW_MS_SAVE_PATH = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET"
RAW_MS_CHOICE_PATH="F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET\model_30_32_1024_lr0.0003_norm\epoch_29_99.7_99.5.pth"

#RawNet48000 模型保存 文件夹
RAW48000_MS_BATCH_SIZE=32
RAW48000_MS_NUM_EPOCHS=30
RAW48000_YAML_CONFIG_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\RawNet48000\model_config_RawNet.yaml"
RAW48000_MS_SAVE_PATH = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET48000"
RAW48000_MS_CHOICE_PATH="F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET48000\model_30_32_1024_lr0.0003_norm\epoch_12_99.4_99.2.pth"

#RawNet40000 模型保存 文件夹
RAW40000_MS_BATCH_SIZE=32
RAW40000_MS_NUM_EPOCHS=30
RAW40000_YAML_CONFIG_PATH=r"F:\Attack-Transferability-On-Synthetic-Detection\RawNet40000\model_config_RawNet.yaml"
RAW40000_MS_SAVE_PATH = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET40000"
RAW40000_MS_CHOICE_PATH="F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET40000\model_30_32_1024_lr0.0003_norm\epoch_12_99.4_99.2.pth"