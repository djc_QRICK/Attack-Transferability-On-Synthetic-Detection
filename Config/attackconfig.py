#这是我用来设置攻击产生的对抗样本的存放的设置
TPATH=r"F:\csy\ASVNormClip64600Dataset\attackdataset3"
TLABEL=r"F:\csy\ASVNormClip64600Dataset\attackdataset3_label.txt"
VMIPATH=r"F:\csy\ASVNormClip40000Dataset\attackdataset1"
VMILABEL=r"F:\csy\ASVNormClip40000Dataset\attackdataset1_label.txt"
# # 1000真样本
# TPATH=r"F:\csy\ASVNormClip64600Dataset\attackdataset2"
# TLABEL=r"F:\csy\ASVNormClip64600Dataset\attackdataset2_label.txt"




#LMCC_LCNN
LFCC_LCNN_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_LFCC\WCE_20_16_0.0001\epoch_19_98.1_98.9.pth"
LFCC_LCNN_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\LFCC_LCNN"
LFCC_LCNN_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\LFCC_LCNN"
LFCC_LCNN_MIFGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\MI-FGSMAdvsave\LFCC_LCNN"

#LMCC_RESNET
LFCC_RESNET_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_LFCC\WCE_20_16_0.0001\epoch_19_99.9_99.8.pth"
LFCC_RESNET_FGSMAdvsetPath =r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\LFCC_RESNET"
LFCC_RESNET_PGDAdvsetPath =r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\LFCC_RESNET"
LFCC_RESNET_MIFGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\MIAdvsave\LFCC_RESNET"
LFCC_RESNET_VMIFGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\LFCC_RESNET"

#LMCC_RESNET48000
LFCC_RESNET48000_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET48000_LFCC\WCE_20_16_0.0001\epoch_19_99.9_99.9.pth"
LFCC_RESNET48000_FGSMAdvsetPath =r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\LFCC_RESNET48000"
LFCC_RESNET48000_PGDAdvsetPath =r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\LFCC_RESNET48000"

#LMCC_RESNET40000
LFCC_RESNET40000_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_LFCC\WCE_20_16_0.0001\epoch_11_99.7_99.8.pth"
LFCC_RESNET40000_FGSMAdvsetPath =r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\LFCC_RESNET40000"
LFCC_RESNET40000_PGDAdvsetPath =r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\LFCC_RESNET40000"
LFCC_RESNET40000_VMIFGSMAdvsetPath =r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\LFCC_RESNET40000"
LFCC_RESNET40000_VMIFGSMAdvsetPath5 =r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\LFCC_RESNET_0.005_40000"
LFCC_RESNET40000_VMIFGSMAdvsetPath2 =r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\LFCC_RESNET_0.002_40000"
LFCC_RESNET40000_VMIFGSMAdvsetPath8 =r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\LFCC_RESNET_0.0008_40000"
LFCC_RESNET40000_VMIFGSMAdvsetPath1 =r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\LFCC_RESNET_0.0001_40000"


LFCC_RESNET40000_MIFGSMAdvsetPath =r"F:\Attack-Transferability-On-Synthetic-Detection\MIFGSMAdvsave\LFCC_RESNET40000"
#LMCC60_LCNN
LFCC60_LCNN_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_LFCC60\WCE_20_16_0.0001\epoch_19_99.3_99.6.pth"
LFCC60_LCNN_FGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\LFCC60_LCNN"

#LMCC60_RESNET
LFCC60_RESNET_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_LFCC60\WCE_20_16_0.0001\epoch_19_99.9_99.6.pth"
LFCC60_RESNET_FGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\LFCC60_RESNET"


#MFCC_LCNN
MFCC_LCNN_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_MFCC\WCE_20_16_0.0001\epoch_19_98.6_98.8.pth"
MFCC_LCNN_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\MFCC_LCNN"
MFCC_LCNN_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\MFCC_LCNN"

#MFCC_RESNET
MFCC_RESNET_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_MFCC\WCE_20_16_0.0001\epoch_19_99.8_99.7.pth"
MFCC_RESNET_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\MFCC_RESNET"
MFCC_RESNET_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\MFCC_RESNET"

MFCC_RESNET_VMIFGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\MFCC_RESNET"

#MFCC80_LCNN
MFCC80_LCNN_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_MFCC80\WCE_20_16_0.0001\epoch_19_98.6_98.8.pth"
MFCC80_LCNN_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\MFCC80_LCNN"
MFCC80_LCNN_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\MFCC80_LCNN"

#MFCC80_RESNET
MFCC80_RESNET_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_MFCC80\WCE_20_16_0.0001\epoch_19_99.9_99.9.pth"
MFCC80_RESNET_FGSMAdvsetPath = r"N:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\MFCC80_RESNET"
MFCC80_RESNET_PGDAdvsetPath=r"N:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\MFCC80_RESNET"

#MFCC_RESNET48000
MFCC_RESNET48000_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET48000_MFCC\WCE_20_16_0.0001\epoch_6_99.6_99.4.pth"
MFCC_RESNET48000_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\MFCC_RESNET48000"
MFCC_RESNET48000_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\MFCC_RESNET48000"

#MFCC_RESNET40000
MFCC_RESNET40000_model = r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_MFCC\WCE_20_16_0.0001\epoch_8_99.6_99.2.pth"
MFCC_RESNET40000_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\MFCC_RESNET48000"
MFCC_RESNET40000_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\MFCC_RESNET40000"
MFCC_RESNET40000_VMIFGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\MFCC_RESNET40000"

#SPEC_LCNN
SPEC_LCNN_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_SPEC\WCE_30_12_0.0001\epoch_10_99.0_99.6.pth"
SPEC_LCNN_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\SPEC_LCNN"
SPEC_LCNN_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\SPEC_LCNN"

#SPEC_RESNET_NEW
SPEC_RESNET_NEW_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_SPEC_NEW\WCE_20_12_0.0001\epoch_19_99.9_99.9.pth"
SPEC_RESNET_NEW_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\SPEC_RESNET_NEW"
SPEC_RESNET_NEW_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\SPEC_RESNET_NEW"
SPEC_RESNET_text_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\spec_resnet_text"
SPEC_RESNET_NEW_VMIFGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\VMI-FGSMAdvsave\SPEC_RESNET_NEW"

#SPEC_RESNET48000
SPEC_RESNET48000_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET48000_SPEC\WCE_20_12_0.0001\epoch_18_99.9_99.9.pth"
SPEC_RESNET48000_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\SPEC_RESNET48000"
SPEC_RESNET48000_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\SPEC_RESNET48000"

#SPEC_RESNET40000
SPEC_RESNET40000_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET40000_SPEC\WCE_20_12_0.0001\epoch_18_99.8_99.9.pth"
SPEC_RESNET40000_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\SPEC_RESNET48000"
SPEC_RESNET40000_PGDAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\SPEC_RESNET40000"

#SPEC1024_LCNN
SPEC1024_LCNN_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\LCNN_SPEC1024\WCE_30_12_0.0001\epoch_10_99.0_99.6.pth"
SPEC1024_LCNN_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\SPEC1024_LCNN"

#SPEC1024_RESNET
SPEC1024_RESNET_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RESNET_SPEC1024\WCE_20_12_0.0001\epoch_19_99.9_99.9.pth"
SPEC1024_RESNET_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\SPEC1024_RESNET"

#rawnet
RawNet_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET\model_30_32_1024_lr0.0003_norm\epoch_29_99.7_99.5.pth"
RawNet_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\RawNet"
RawNet_PGDAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\RawNet"

RawNet_VMIFGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\RawNet"
RawNet_MIFGSMAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\MIAdvsave\RawNet"
RawNet_CWAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\CWAdvsave\RawNet"
RawNet_DeepfoolAdvsetPath=r"F:\Attack-Transferability-On-Synthetic-Detection\DeepfoolAdvsave\RawNet"
#rawnet48000
RawNet48000_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET48000\model_30_32_1024_lr0.0003_norm\epoch_15_99.2_99.2.pth"
RawNet48000_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\RawNet48000"
RawNet48000_PGDAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\RawNet48000"

#rawnet40000
RawNet40000_model=r"F:\Attack-Transferability-On-Synthetic-Detection\ALL_MODEL\RAWNET40000\model_30_32_1024_lr0.0003_norm\epoch_25_99.0_98.1.pth"
RawNet40000_FGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\FGSMAdvsave\RawNet40000"
RawNet40000_PGDAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\PGDAdvsave\RawNet40000"
RawNet40000_VMIFGSMAdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\VMIAdvsave\RawNet40000"

#ENS64600PGDpath:
ENS64600_AdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\ENSAdvsave\ENS64600"
ENS48000_AdvsetPath = r"F:\Attack-Transferability-On-Synthetic-Detection\ENSAdvsave\ENS48000"