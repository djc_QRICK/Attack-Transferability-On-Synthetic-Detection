import ASVBaselineTool.trans_tool as tran
import Config.attackconfig as Config
import Config.globalconfig as Gconfig
import torch
import yaml
from RawNet.model import RawNet

model_path = Config.RawNet_model
advsavepath =Config.LFCC_RESNET_PGDAdvsetPath
cleandatapath = Config.TPATH
trainlabelpath = Config.TLABEL

device = 'cuda' if torch.cuda.is_available() else 'cpu'
# 模型内部结构配置
yaml_path = Gconfig.RAW_YAML_CONFIG_PATH
with open(yaml_path, 'r') as f_yaml:
    parser1 = yaml.load(f_yaml, Loader=yaml.FullLoader)
#
model = RawNet(parser1['model'], device)
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))


A = tran.CalTransAtKSuccess(model,advsavepath,cleandatapath,trainlabelpath,feature= "raw",desc="LFCC_RESNET")
print(A)