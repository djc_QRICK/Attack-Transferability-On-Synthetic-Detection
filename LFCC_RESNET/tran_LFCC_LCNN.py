import ASVBaselineTool.trans_tool as tran
import Config.attackconfig as Config
import torch
from LFCC_LCNN.model import Model_zoo

model_path = Config.LFCC_LCNN_model
advsavepath =Config.LFCC_RESNET_PGDAdvsetPath
cleandatapath = Config.TPATH
trainlabelpath = Config.TLABEL

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("lcnn_lfcc")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))


A = tran.CalTransAtKSuccess(model,advsavepath,cleandatapath,trainlabelpath,feature= "lfcc_70",desc="LFCC_LCNN")
print(A)