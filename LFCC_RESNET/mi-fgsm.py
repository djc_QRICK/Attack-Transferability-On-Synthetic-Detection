import ASVBaselineTool.MIFGSM as attack
import Config.attackconfig as Config
import torch
from LFCC_RESNET.model import Model_zoo

model_path = Config.LFCC_RESNET_model
labelPath = Config.TLABEL
datasetPath = Config.TPATH
AdvsetPath = Config.LFCC_RESNET_MIFGSMAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.MIFGSM(model,labelPath,datasetPath,AdvsetPath,steps=40,eps=0.08)
feature = "lfcc_70"
a.Attack(feature,alpha=0.001,stop=0.05)
