import ASVBaselineTool.trans_tool as tran
import Config.attackconfig as Config
import torch
from LFCC_RESNET48000.model import Model_zoo

model_path = Config.LFCC_RESNET48000_model
advsavepath =Config.MFCC_RESNET_PGDAdvsetPath
cleandatapath = Config.TPATH
trainlabelpath = Config.TLABEL

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))


A = tran.CalClipTransAtKSuccess(model,64600,48000,advsavepath,cleandatapath,trainlabelpath,feature= "lfcc_70",desc="LFCC_RESNET48000")
print(A)