import ASVBaselineTool.VMIFGSM as attack
import Config.attackconfig as Config
import torch
from MFCC_RESNET.model import Model_zoo

model_path = Config.MFCC_RESNET_model
labelPath = Config.TLABEL
datasetPath = Config.TPATH
AdvsetPath = Config.MFCC_RESNET_VMIFGSMAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.VMIFGSM(model,labelPath,datasetPath,AdvsetPath,steps=40,eps=0.08)
feature = "mfcc_40"
a.Attack(feature,alpha=0.001,stop=0.05)
