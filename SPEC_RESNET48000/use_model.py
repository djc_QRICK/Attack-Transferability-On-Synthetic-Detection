import torch
import Config.globalconfig as Gconfig
from SPEC_RESNET48000.model import Model_zoo
import torchaudio
from ASVBaselineTool.SPEC_TOOL import get_torch_spectrum
def getSavedModel():
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    #已有的模型路径
    MODEL_SAVE_PATH=Gconfig.SPECRESNET48000_MS_SAVE_PATH
    # 模型内部结构配置
    model = Model_zoo("ResNet18")
    model = (model).to(device)
    model.load_state_dict(torch.load(MODEL_SAVE_PATH, map_location=device))
    print('Model loaded : {}'.format(MODEL_SAVE_PATH))
    return model

def demo():
    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print(device)
    model=getSavedModel()
    model.train()
    x=torch.randn(1,40000)
    x=get_torch_spectrum(x)
    assert x.shape==torch.Size([1,1025,94])
    #x = torch.transpose(x, 1, 2)
    #assert x.shape==torch.Size([1,324,70])
    if device=="cuda":
        x=x.cuda()
    batch_x=model(x)
    print(batch_x)
    print(batch_x.shape)
    print("转换成概率")
    print("变成（-1，2）")
    batch_x=batch_x.reshape(-1)
    som=torch.nn.Softmax()
    y=som(batch_x)
    print(y)
    print(y.shape)

if __name__=="__main__":
    demo()