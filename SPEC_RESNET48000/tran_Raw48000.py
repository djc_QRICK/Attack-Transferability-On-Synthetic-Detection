import ASVBaselineTool.trans_tool as tran
import Config.attackconfig as Config
import Config.globalconfig as Gconfig
import torch
import yaml
from RawNet48000.model import RawNet

model_path = Config.RawNet48000_model
advsavepath =Config.SPEC_RESNET_PGDAdvsetPath
cleandatapath = Config.TPATH
trainlabelpath = Config.TLABEL

device = 'cuda' if torch.cuda.is_available() else 'cpu'
# 模型内部结构配置
yaml_path = Gconfig.RAW_YAML_CONFIG_PATH
with open(yaml_path, 'r') as f_yaml:
    parser1 = yaml.load(f_yaml, Loader=yaml.FullLoader)
#
model = RawNet(parser1['model'], device)
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))


A = tran.CalClipTransAtKSuccess(model,64600,48000,advsavepath,cleandatapath,trainlabelpath,feature= "raw",desc="RAW48000")
print(A)