import ASVBaselineTool.PGDATTACK as attack
import Config.attackconfig as Config
import torch
from  SPEC_RESNET40000.model import Model_zoo

model_path = Config.SPEC_RESNET40000_model
labelPath = Config.VMILABEL
datasetPath = Config.VMIPATH
AdvsetPath = Config.SPEC_RESNET40000_PGDAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.PGD(model,labelPath,datasetPath,AdvsetPath,steps=40,eps=0.08)
feature = "spec_2048"
a.Attack(feature,alpha=0.001,stop=0.05)
