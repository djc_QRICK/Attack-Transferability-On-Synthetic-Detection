import numpy as np
import ASVBaselineTool.PGDATTACK as attack
import Config.attackconfig as Config
import Config.globalconfig as Gconfig
import torch
from RawNet40000.model import RawNet
import yaml

model_path = Config.RawNet40000_model
labelPath = Config.VMILABEL
datasetPath = Config.VMIPATH
AdvsetPath = Config.RawNet40000_PGDAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
yaml_path = Gconfig.RAW40000_YAML_CONFIG_PATH
with open(yaml_path, 'r') as f_yaml:
    parser1 = yaml.load(f_yaml, Loader=yaml.FullLoader)
model = RawNet(parser1['model'], device)
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.PGD(model,labelPath,datasetPath,AdvsetPath,steps=60,eps=0.08)
feature = "raw"
a.Attack(feature,alpha=0.001,stop=0.05)
