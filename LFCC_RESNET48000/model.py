import torch
from torch import nn
import torchaudio
from torchvision import models

class Model_zoo(nn.Module):
    def __init__(self, mt, device=None, parser1=None):
        super(Model_zoo, self).__init__()
        self.mt = mt
        self.device = device
        self.n_m = {
            # "shuffleNetV2": models.shufflenet_v2_x0_5(False, True),
            #"DensenNet121": models.densenet121(False, True),
            # "MobileNetV2": models.mobilenet_v2(False, True),
            "ResNet18": models.resnet18(False, True),
        }
        self.model = self.n_m[self.mt]
        self.conv = nn.Conv2d(1, 3, kernel_size=3, stride=1, padding=1)
        self.fc = nn.Linear(1000, 2)


    # x input shape = (64600)
    def forward(self, x, Freq_aug=False):
        x = x.unsqueeze(dim=1)# (1,mmmm,num_filter)
        # 将1d矩阵变成3D矩阵
        x = self.conv(x)#(1,1,mmm,num_filter)
        x = self.model(x)
        # 将1000分类变成2分类
        x = self.fc(x)

        return x

from ASVBaselineTool.DEMONEED import MFCCfeature,CQCCfeature,LFCCfeature48000,SPECfeature
def demo():
    #测试resnet的网络畅通性
    for feature in [LFCCfeature48000]:
        net = Model_zoo("ResNet18")
        print(net(feature).shape)
        print("ResNet18 模型 lfcc 特征输出正常")



if __name__=="__main__":
    demo()