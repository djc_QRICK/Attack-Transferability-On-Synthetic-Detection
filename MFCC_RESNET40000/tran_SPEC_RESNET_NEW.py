import ASVBaselineTool.trans_tool as tran
import Config.attackconfig as Config
import torch
from SPEC_RESNET_NEW.model import Model_zoo

model_path = Config.SPEC_RESNET_NEW_model
advsavepath =Config.MFCC_RESNET40000_PGDAdvsetPath
cleandatapath = Config.VMIPATH
trainlabelpath = Config.VMILABEL

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))


A = tran.CalClip40000to64600TransAtKSuccess(model,40000,64600,advsavepath,cleandatapath,trainlabelpath,feature='spec_2048',desc='SPEC_RESNET')
print(A)