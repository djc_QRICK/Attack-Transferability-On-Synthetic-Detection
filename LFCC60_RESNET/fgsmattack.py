import numpy as np
import ASVBaselineTool.FGSMATTACK as attack
import Config.attackconfig as Config
import torch
from LFCC60_RESNET.model import Model_zoo

model_path = Config.LFCC60_RESNET_model
labelPath = Config.TLABEL
datasetPath = Config.TPATH
AdvsetPath = Config.LFCC60_RESNET_FGSMAdvsetPath

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))

a = attack.FRAMEFGSM(model,labelPath,datasetPath,AdvsetPath)
feature = "lfcc_60"
for eps in np.linspace(0.015,0.028,20):
    a.Attack(feature,eps=eps)
