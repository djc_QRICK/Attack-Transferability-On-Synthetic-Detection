import ASVBaselineTool.trans_tool as tran
import Config.attackconfig as Config
import torch
from MFCC30_RESNET.model import Model_zoo

model_path = Config.MFCC30_RESNET_model
advsavepath =Config.SPEC_RESNET_NEW_PGDAdvsetPath
cleandatapath = Config.TPATH
trainlabelpath = Config.TLABEL

device = 'cuda' if torch.cuda.is_available() else 'cpu'
print(device)
model = Model_zoo("ResNet18")
model = (model).to(device)
model.load_state_dict(torch.load(model_path, map_location=device))
print('Model loaded : {}'.format(model_path))


A = tran.CalTransAtKSuccess(model,advsavepath,cleandatapath,trainlabelpath,feature= "mfcc_30",desc="MFCC_RESNET")
print(A)